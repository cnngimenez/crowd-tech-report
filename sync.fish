#! /usr/bin/fish


# Copyright 2018 Giménez, Christian

# Author: Giménez, Christian   

# sync.fish

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


function sync-tech-report --description="Sync files to the tech-report server"
	rsync -av --exclude=".hg" -e "ssh -p 22" *.txt *.html *.pdf ltximg images output crowd@crowd.fi.uncoma.edu.ar:~/tech-report	
end


