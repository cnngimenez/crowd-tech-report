This is the *crowd* tecnical report.


# Requirements

-   [Mercurial](https://www.mercurial-scm.org/) (`hg` command) for cloning repository.

-   Emacs
-   Org-mode
-   LaTeX
-   ImageMagik
-   dia
-   [fish shell](https://fishshell.com/)

LaTeX packages needed:

-   capt-of
-   ulem
-   wrapfig
-   tikz


# Compiling

1.  Execute `compile-dia.fish` script for creating images.
2.  Open Emacs and open an Org file.
3.  Type `C-c C-e` and select the type (for creating and compiling LaTeX, use `C-c C-e l p`).

For compiling Markdown, load the library ox-md: `M-x load-library RET ox-md RET`.


## Literate Org

For creating code files, see `org-babel-tangle` documentation (execute with `C-c C-v t`).

