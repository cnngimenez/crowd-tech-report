
More content:

- [[file:introduction.org][Introduction and related works]].
- [[file:uml-like%20vql.org][UML-Like Visual Query Language proposed]].
- [[file:crowd%20implementation.org][crowd Implementation]].

* Introduction
Current engineering onthologies utilities provides a mean for querying instance data. Worth to mention technologies that applies Onthology Based Data Access technique, where in this case large amounts of information is stored in relational databases for latter processing. Examples of these tools can be divided in storing or relational-to-OWL/RDF mapping software like OpenLink Virtuoso, Sesame, D2RQ and for querying: OptiqueVQS, OntoVQL, SPARQL reasoners, SPARQL-DL, OWLlink/DIG reasoners, etc.

Nevertheless, some domains require information to be stored in a more flexible environment where databases tend to be a more complicated solution. In such a case, using onthologies with reasoners is usually a solution. Under these situations, quering only the data instances may not be enough for the user as also the structure are dynamic and usable for reasoning.

# Little motivation, why we are doing this?
/crowd/ Web application posses the hability to create models that represents the structural part of an onthology. However, no query can be performed to consult part of the  explicit or implicit structure. 

# Are there anything that resembles what we want to achieve? In what is better our research/technology comparing to other's?
There are languages for querying onthologies that uses textual input like ask-sentence from OWLlink and DIG, SPARQL and SPARQL-DL. Also, there are visual querying languages and systems that easier the input for the user. All these languages lack the expressiveness for querying instances and structure of an onthology.


To our knowledge, no visual query language based in SPARQL-DL exists nor are implemented in a tool. 

# What we are going to explain here? Maybe some introduction about crowd sparql-dl extension?
In this part we explain a /crowd/ extension that implements a SPARQL-DL based visual query language which graphical primitives are simmilar to UML's ones. 

# What applications? Why is important? What can be used this for?
This make possible future applications like searchin for patterns and antipatterns in the onthology structure and for helping developers in making queries easily by modelling a UML-like model.

** Difference between modelling languages and ontologies
Ontologies and modelling languages may be able to represent the same domains. However, ontologies are descriptive and models are prescriptive. "Descriptive" means that they describe what already exists. In the latter, it prescribe systems which do not exists and pertain to implementation of them. 

In other words, models are forward looking solutions artefacts, while ontologies are backward looking problem domain artefacts. Hence, ontologies are essencially meant to be richer and closer to end-user vocabulary and understanding that database schemas.

Consequently, a visual query tool built on ontologies provides a closer representation of the problem domain, hence enabling users to understand and communicate better cite:Soylu17_VQ,ruiz06:_using_ontol_softw_engin_techn.

** Why not using Structured Query Languages
Structured Query Languages such as SQL an XQuery requires technical skills and knowledge on query language, syntax and domain schema. End users has to express their information needs in a programatic way. These apporaches make database systems almost inaccessible to end users.  

Using these languages results in the needs of an intermediary role such as IT experts. This role objective is formulating queries for the users or domain experts. The query construction process comes with a cost of turnaround time aside from the direct cost of the IT experts. 

Figure ref:img:trad-acc depicts two traditional data access models. End users cannot interact with databases or knowledge bases directly because of the technical skills required. For this reason, the end user are dependent on IT experts who provide a predefined set of queries or create specialized queries. The simple case of data access models does not anticipate every information need in advance. Moreover, in the complex case, the IT experts collaborate with users to create new specialized queries. This case, often results in repetitive query reformulations in order to resolve possible misinterpretations in the information needs. 

#+caption: Traditional data access models. label:img:trad-acc 
[[file:../images/traditional-data-access-scenarios-small.png]]

For this reason, the challenge is to reduce costs on the query reformulation and evaluation times. One of the approaches is by enabling users to create their own queries cite:Soylu17_VQ.

** Visual Query Formulation
The purpose of a visual query formulation tool is facilitate retrieval of data. Visual query formulation approaches exploit the bi-directional and the multi-sensory characteristics of visual representations. For example, the user reads information through a number of sensory variables such as texture, color, size and shape. The concern is essencially about the productivity and the quality of human-machine interaction rather than the functionality or the technology of the software.

* VQS and VQL

** Evaluation of VQ
According to cite:Soylu17_VQ, usability and expressivenes are the most important dimensions for visual query formulation. It is relevant to focus on these two software qualities.

*** TODO Usability
*** TODO Expressivenes
** Presentation and Interaction
Interaction refers to a control-feedback loop between the user and the application. Expressiveness and usability are bi-directional.

One direction is considered to begin with the system or language to the  user point of view. It corresponds to the quality to represent the domain knowledge and elicited information needs. For instance, the language quality to represent subsumption and disjointness between domain concepts.

The other one, is from the user to the system or language point of view. It is important to express the user's information needs through the system or language. For example, the allowance of the language to let the user to express conjuctive and disjunctive queries.

** Prototype and Implementation Consideration                   :convention:
The new implementation must allow an interactive control-feedback loop between the user and the system. 

For example, to edit the query interactively using a VQL. When the query is ready, to show an example of the results in the lower margin. Through this way, the user can create queries in incremental steps.

It must contemplate the language expressivenes to display the most relevant data to the user. Moreover, to allow them to query as much expressive as possible.


* TODO Meta-VQL
A Meta-VQL is a Visual Language that defines elements or "meta-primitives" that can be replaced by another graphical representation. Instances of a Meta VQL are VQLs that has all its meta-primitives defined.

A Meta-VQL instance can use graphical representations that are more understandable by domain experts. For example, a meta-predicate called "Industry" can be replaced by a logo or a real industry picture. 

* Applications
Visual techniques for accessing data facilitate the design of queries for users and developers, reducing the learning curve and also abstract the complexity of the underlying logical query language. Moreover, it is possible to use the UML-like syntax for searching ontology patterns  cite:Staab:Ontologies and anti-patterns cite:Sales:Antipatterns,Roussey:Antipatterns represented in a visual context and thus helping the engineer to raise the quality of the ontology design.
In the same direction, other applications are related to the bottom-up construction of ontologies by means of non-standard services such as last common subsumers and most specific concepts cite:Baader:2003:LCS:1630659.1630706. Lastly, reasoners can also search for a possible pattern in a top-down way by querying the existence of part of the pattern into the input ontology, and thus suggesting the missing elements which completes them cite:KF:patterns,DBLP:conf/jaiio/BraunC15. For example, consider the pattern "List 2" presented in Figure ref:fig:pattern-list. Part of the pattern is queried by using the UML class diagram depicted in Figure ref:fig:pattern-list-uml into the input ontology. The missing elements of the pattern can be suggested to the user for appending to the input ontology according to the results of the query.

#+caption: List pattern obtained from cite:odp-list. label:fig:pattern-list
#+name: fig:pattern-list
#+attr_latex: width=.7\textwidth
[[file:../images/List-pattern.png]]

#+begin_src latex :file output/pattern-list-uml.png :imagemagick t :border 1em :results raw :headers '("\\usepackage{tikz}" "\\usetikzlibrary{shapes.geometric}" "\\usetikzlibrary{shapes.symbols}" "\\usetikzlibrary{positioning}" "\\usetikzlibrary{arrows.meta}" "\\usetikzlibrary{trees}")
\begin{tikzpicture}
    \node[draw, inner sep=5] (A) {?ListItem};
  \draw[arrows=->] (A.south east) .. controls (2.5,-2) and (2.5,2) .. (A.north east) node[below, near start] {hasNext};
\end{tikzpicture}
#+end_src
#+caption: The UML-like query used for searching the pattern. label:fig:pattern-list-uml
#+name: fig:pattern-list-uml
#+attr_latex: :width 4cm
#+RESULTS:
[[file:output/pattern-list-uml.png]]

#+begin_src latex :file output/anti-pattern-SOE.png :imagemagick t :border 1em :results raw :headers '("\\usepackage{tikz}" "\\usetikzlibrary{shapes.geometric}" "\\usetikzlibrary{shapes.symbols}" "\\usetikzlibrary{positioning}" "\\usetikzlibrary{arrows.meta}" "\\usetikzlibrary{trees}")
\begin{tikzpicture}
  %% Class
  \node[draw,inner sep=5] (C1) {?C1};
  \node[draw,inner sep=5] (C2) [right=2 of C1] {?C2};

  \draw[thick, arrows={Triangle[open, width=10pt, length=10pt]-}, bend right]
  (C1) edge (C2);
  \draw[thick, arrows={Triangle[open, width=10pt, length=10pt]-}, bend right]
  (C2) edge (C1);
\end{tikzpicture}
#+end_src
#+caption: UML-like diagram for querying the expression $C1 \equiv C2$. label:fig:antipattern-SOE-UML
#+name: fig:antipattern-SOE-UML
#+attr_latex: :width 4cm
#+RESULTS:
[[file:output/anti-pattern-SOE.png]]

Searching for anti-patterns represented in a graphical context can avoid situations that normally result in inconsistencies. This anti-patterns can be imported or drawn as a query in a UML-like diagram, making it easier to the user to remember and to understand. For example, searching for the logical anti-pattern called "Synonym or Equivalence" (SOE) explained in cite:antipattern:SOE requires to look into the ontology for the expression $C1 \equiv C2$, which latter should be treated by the user removing C2 according to the pattern solution. The Figure ref:fig:antipattern-SOE-UML displays a UML representation for the expression.

* Glossary
In this section, a list of terms is presented along with their definitions. They are considered important or are defined in a slightly different manner as usual. The references to the paper is provided along with the quotation or a short explanation.

- Collaboration :: Unambiguous communication medium for human-human dialogs. Ontologies have got the potential to act as a collaboration medium cite:Soylu17_VQ.
- Interoperability :: Unambiguous communication medium for machine-machine dialogs. Ontologies have also got the potential to act as an interoperable medium cite:Soylu17_VQ.
- Alignment :: 
- WYSIWYQ :: It means What You See Is What You Query. Is mentioned at cite:cerans:ViziQuer and described cite:Khalili:WYSIWYQ.
- Information Needs ::
- Data Access :: 
- Query Formulation :: (Or query construction)
- Visual Query Formulation ::

- Query Evaluation :: 

- Ontology-based Data Access :: (OBDA)

- Data retrieval :: In DR an information need must be exact and complete. It is defined over a deterministic model with the aim of retrieing all and only those objects that exactly match the criteria. There are Visual DR techniques: VQL and VQS.

- Information retrieval :: Information needs are typically incomplete and loosely defined.

** Difference between models and ontologies
There are differences between models and ontologies. Interestingly, each author use different terms like "conceptual modelling", "data models" or simply "models".



- Model :: "Is defined as an abstraction representing some view of the reality, by necessarily omitting details, intended for a definite purpose" cite:Soylu17_VQ,ruiz06:_using_ontol_softw_engin_techn,. Also, models is considered as /prescriptive/. They prescribe non-existant systems that is going to be implemented. Models looks for a /solution/implementation domain/ artefacts
- Ontologies :: cite:Soylu17_VQ,ruiz06:_using_ontol_softw_engin_techn, mention that ontologies are /descriptive/. This means that ontologies describes what kind of information exists now. Hence, they are richer and closer to domain vocabulary than database schemas.

- Data Model :: "Represents the structure and integrity of the data elements of the, in principle "single", specific enterprise application(s) by which it will be used."   cite:spyns02:_data_model_versus_ontol_engin A data model is not to be shared between applications usually.
- Computer Ontology :: Exist several definitions of ontologies in the scientific literature. cite:spyns02:_data_model_versus_ontol_engin quote the following: "an agreement about a shared, formal explicit and partial account of a conceptualization" and adds that it contains a vocabulary and the definition of the concepts and their relationships for a given domain. Furthermore, in various ontologies, instances and rule are included.

** Approaches for Quering Semantic Data Sources
The following concepts are explained at cite:Soylu:Ontology-basedVQ. They are considered as differents approaches to query semantic data sources. This sections explains them and/or describes their advantages and disadvantages.

- Formal Textual Languages :: This approach is inaccessible to end users, since they demand a sound technical background.
- Keword Search :: Remain insufficient for querying structured data due to low expressiveness.
- Natural Language Interfaces :: Remain insufficient for querying structured data due to ambiguities.
- Visual Query Languages :: It is based on a formal visual syntax and notation. It is comparable to formal textual languages from an end-user perspective, as users need to understand the semantics of visual syntax and notation.
- Visual Query Systems :: It is primarily a system of interactions formed by an arbitrary set of user actions that effectively capture a set of syntactic rules specifying a (query) language.

A VQS might use a VQL for query representation; however, VQSs built on non-formal visualisation are expected to offer a good usability-expressiveness balance.

VQS undertake the challenge of making querying independent of users' technical skills and the knowledge of the underlying textual query language and the structure of data.

** VQS Approaches

- VQL-based :: This category includes approaches that are primarily built on a VQL, which has a formal visual syntax and notation.
- Interactive-based :: It mainly employs a system of interactions, i.e. VQSs which generates queries in target linguistic form.
  - Diagram-based :: Are good in providing a global overview. However, they remain insufficient for view. This is because the visual space as a whole is mostrly occupied for query overview.
  - Form-based :: Provides a good view. However, they provide a poor overview, since the visual space as a whole is mostly occupied with the properties of the focus concept.
  - Multiple :: Combine multiple representation and interaction paradigms. The can combine view and overview.

Albeit VQL-based approaches with higher level of abstraction are closer to end users, they still need to posses a higher level of knowledge and skills to understand the semantics of visual notation and syntax and to use it.

* References
<<bibliography link>>

bibliographystyle:plain
bibliography:../biblio.bib




* Meta     :noexport:

  # ----------------------------------------------------------------------
  #+TITLE:  Introducing Visual Query Languages to crowd
  #+AUTHOR: Christian Gimenez
  #+DATE:   12 Aug 2019
  #+EMAIL:
  #+DESCRIPTION: 
  #+KEYWORDS: 

  #+STARTUP: inlineimages hidestars content hideblocks entitiespretty indent fninline latexpreview
  #+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)
  #+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
  #+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc tex:imagemagick
  #+LINK_UP: ../index.org
  #+LINK_HOME: ../index.org
  #+XSLT:

  # -- HTML Export
  #+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
  #+EXPORT_SELECT_TAGS: export
  #+EXPORT_EXCLUDE_TAGS: noexport
  #+HTML_LINK_UP: ../index.html
  #+HTML_LINK_HOME: ../index.html

  # -- For ox-twbs or HTML Export
  #+HTML_HEAD: <link href="../libs/bootstrap.min.css" rel="stylesheet">
  #+HTML_HEAD: <script src="../libs/jquery.min.js"></script> 
  #+HTML_HEAD: <script src="../libs/bootstrap.min.js"></script>
  #+LANGUAGE: en

  # Local Variables:
  # org-hide-emphasis-markers: t
  # org-use-sub-superscripts: "{}"
  # fill-column: 80
  # visual-line-fringe-indicators: t
  # ispell-local-dictionary: "british"
  # End:
