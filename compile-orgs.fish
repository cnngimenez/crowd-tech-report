#! /usr/bin/fish


# Copyright 2018 Giménez, Christian

# Author: Giménez, Christian   

# compile-orgs.fish

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


for f in (ls -1 *.org)
	echo Exporting $f
	emacs "$f" --batch -f org-html-export-to-html --kill >/dev/null ^/dev/null
	emacs "$f" --batch -f org-latex-export-to-pdf --kill >/dev/null ^/dev/null
end


